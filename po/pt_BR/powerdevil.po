# Translation of powerdevil.po to Brazilian Portuguese
# Copyright (C) 2014-2020 This file is copyright:
# This file is distributed under the same license as the powerdevil package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2014, 2015, 2016, 2019, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2016, 2017, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-15 02:12+0000\n"
"PO-Revision-Date: 2022-12-15 11:32-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "André Marcelo Alvarenga, Luiz Fernando Ranghetti"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alvarenga@kde.org, elchevive@opensuse.org"

#: actions/bundled/brightnesscontrol.cpp:53 actions/bundled/dpms.cpp:76
#: actions/bundled/handlebuttonevents.cpp:57
#: actions/bundled/keyboardbrightnesscontrol.cpp:57
#, kde-format
msgctxt "Name for powerdevil shortcuts category"
msgid "Power Management"
msgstr "Gerenciamento de energia"

#: actions/bundled/brightnesscontrol.cpp:56
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Screen Brightness"
msgstr "Aumentar o brilho da tela"

#: actions/bundled/brightnesscontrol.cpp:61
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Screen Brightness"
msgstr "Reduzir o brilho da tela"

#: actions/bundled/brightnesscontrolconfig.cpp:75
#, kde-format
msgctxt "Brightness level, label for the slider"
msgid "Level"
msgstr "Nível"

#: actions/bundled/dimdisplayconfig.cpp:64 actions/bundled/dpmsconfig.cpp:60
#: actions/bundled/runscriptconfig.cpp:82
#: actions/bundled/suspendsessionconfig.cpp:86
#, kde-format
msgid " min"
msgstr " min"

#: actions/bundled/dimdisplayconfig.cpp:74
#: actions/bundled/runscriptconfig.cpp:85
#: actions/bundled/runscriptconfig.cpp:106
#, kde-format
msgid "After"
msgstr "Após"

#: actions/bundled/dpms.cpp:79
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Turn Off Screen"
msgstr "Desligar a tela"

#: actions/bundled/dpmsconfig.cpp:61
#, kde-format
msgid "Switch off after"
msgstr "Desligar após"

#: actions/bundled/handlebuttonevents.cpp:62
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Suspend"
msgstr "Suspender"

#: actions/bundled/handlebuttonevents.cpp:67
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Hibernate"
msgstr "Hibernar"

#: actions/bundled/handlebuttonevents.cpp:72
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Power Off"
msgstr "Desligar"

#: actions/bundled/handlebuttonevents.cpp:86
#, kde-format
msgctxt ""
"@action:inmenu Global shortcut, used for long presses of the power button"
msgid "Power Down"
msgstr "Desligar"

#: actions/bundled/handlebuttoneventsconfig.cpp:87
#, kde-format
msgctxt "Execute action on lid close even when external monitor is connected"
msgid "Even when an external monitor is connected"
msgstr "Mesmo quando um monitor externo estiver conectado"

#: actions/bundled/handlebuttoneventsconfig.cpp:97
#, kde-format
msgid "Do nothing"
msgstr "Não fazer nada"

#: actions/bundled/handlebuttoneventsconfig.cpp:99
#: actions/bundled/suspendsessionconfig.cpp:90
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Suspender"

#: actions/bundled/handlebuttoneventsconfig.cpp:102
#: actions/bundled/suspendsessionconfig.cpp:93
#, kde-format
msgid "Hibernate"
msgstr "Hibernar"

#: actions/bundled/handlebuttoneventsconfig.cpp:105
#: actions/bundled/suspendsessionconfig.cpp:96
#, kde-format
msgid "Hybrid sleep"
msgstr "Suspensão híbrida"

#: actions/bundled/handlebuttoneventsconfig.cpp:107
#: actions/bundled/suspendsessionconfig.cpp:98
#, kde-format
msgid "Shut down"
msgstr "Desligar"

#: actions/bundled/handlebuttoneventsconfig.cpp:108
#: actions/bundled/suspendsessionconfig.cpp:99
#, kde-format
msgid "Lock screen"
msgstr "Bloquear tela"

#: actions/bundled/handlebuttoneventsconfig.cpp:110
#, kde-format
msgid "Prompt log out dialog"
msgstr "Mostrar janela de encerramento"

#: actions/bundled/handlebuttoneventsconfig.cpp:112
#, kde-format
msgid "Turn off screen"
msgstr "Desligar a tela"

#: actions/bundled/handlebuttoneventsconfig.cpp:138
#, kde-format
msgid "When laptop lid closed"
msgstr "Ao fechar a tampa do laptop"

#: actions/bundled/handlebuttoneventsconfig.cpp:149
#, kde-format
msgid "When power button pressed"
msgstr "Ao apertar o botão de energia"

#: actions/bundled/keyboardbrightnesscontrol.cpp:61
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Keyboard Brightness"
msgstr "Aumentar a iluminação do teclado"

#: actions/bundled/keyboardbrightnesscontrol.cpp:66
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Keyboard Brightness"
msgstr "Reduzir a iluminação do teclado"

#: actions/bundled/keyboardbrightnesscontrol.cpp:71
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Toggle Keyboard Backlight"
msgstr "Alternar a iluminação do teclado"

#: actions/bundled/keyboardbrightnesscontrolconfig.cpp:77
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Level"
msgstr "Nível"

#: actions/bundled/powerprofileconfig.cpp:84
#: actions/bundled/wirelesspowersavingconfig.cpp:75
#: actions/bundled/wirelesspowersavingconfig.cpp:80
#: actions/bundled/wirelesspowersavingconfig.cpp:85
#, kde-format
msgid "Leave unchanged"
msgstr "Manter inalterado"

#: actions/bundled/powerprofileconfig.cpp:92
#, kde-format
msgid "Power Save"
msgstr "Economia de energia"

#: actions/bundled/powerprofileconfig.cpp:93
#, kde-format
msgid "Balanced"
msgstr "Balanceado"

#: actions/bundled/powerprofileconfig.cpp:94
#, kde-format
msgid "Performance"
msgstr "Desempenho"

#: actions/bundled/powerprofileconfig.cpp:109
#, kde-format
msgctxt "Switch to power management profile"
msgid "Switch to:"
msgstr "Mudar para:"

#: actions/bundled/runscriptconfig.cpp:71
#, kde-format
msgid "Script"
msgstr "Script"

#: actions/bundled/runscriptconfig.cpp:83
#, kde-format
msgid "On Profile Load"
msgstr "Ao carregar o perfil"

#: actions/bundled/runscriptconfig.cpp:84
#, kde-format
msgid "On Profile Unload"
msgstr "Ao fechar o perfil"

#: actions/bundled/runscriptconfig.cpp:95
#, kde-format
msgid "Run script"
msgstr "Executar script"

#: actions/bundled/suspendsessionconfig.cpp:87
#, kde-format
msgid "after "
msgstr "após "

#: actions/bundled/suspendsessionconfig.cpp:108
#, kde-format
msgid "Automatically"
msgstr "Automaticamente"

#: actions/bundled/suspendsessionconfig.cpp:114
#, kde-format
msgid "While asleep, hibernate after a period of inactivity"
msgstr "Enquanto suspenso, hibernar ao fim de um período de inatividade"

#: actions/bundled/wirelesspowersavingconfig.cpp:76
#: actions/bundled/wirelesspowersavingconfig.cpp:81
#: actions/bundled/wirelesspowersavingconfig.cpp:86
#, kde-format
msgid "Turn off"
msgstr "Desligar"

#: actions/bundled/wirelesspowersavingconfig.cpp:77
#: actions/bundled/wirelesspowersavingconfig.cpp:82
#: actions/bundled/wirelesspowersavingconfig.cpp:87
#, kde-format
msgid "Turn on"
msgstr "Ligar"

#: actions/bundled/wirelesspowersavingconfig.cpp:110
#, kde-format
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: actions/bundled/wirelesspowersavingconfig.cpp:111
#, kde-format
msgid "Mobile broadband"
msgstr "Banda larga móvel"

#: actions/bundled/wirelesspowersavingconfig.cpp:112
#, kde-format
msgid "Bluetooth"
msgstr "Bluetooth"

#: backends/upower/login1suspendjob.cpp:79
#: backends/upower/upowersuspendjob.cpp:69
#, kde-format
msgid "Unsupported suspend method"
msgstr "Método de suspensão não suportado"

#: powerdevilapp.cpp:62
#, kde-format
msgid "KDE Power Management System"
msgstr "Sistema de gerenciamento de energia do KDE"

#: powerdevilapp.cpp:63
#, kde-format
msgctxt "@title"
msgid ""
"PowerDevil, an advanced, modular and lightweight power management daemon"
msgstr ""
"PowerDevil, um daemon de gerenciamento de energia leve, avançado e modular"

#: powerdevilapp.cpp:65
#, kde-format
msgctxt "@info:credit"
msgid "(c) 2015-2019 Kai Uwe Broulik"
msgstr "(c) 2015-2019 Kai Uwe Broulik"

#: powerdevilapp.cpp:66
#, kde-format
msgctxt "@info:credit"
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: powerdevilapp.cpp:67
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer"
msgstr "Mantenedor"

#: powerdevilapp.cpp:69
#, kde-format
msgctxt "@info:credit"
msgid "Dario Freddi"
msgstr "Dario Freddi"

#: powerdevilapp.cpp:70
#, kde-format
msgctxt "@info:credit"
msgid "Previous maintainer"
msgstr "Mantenedor anterior"

#: powerdevilapp.cpp:160
#, kde-format
msgid "Replace an existing instance"
msgstr "Substituir uma instância existente"

#: powerdevilcore.cpp:436 powerdevilcore.cpp:448
#, kde-format
msgid "Activity Manager"
msgstr "Gerenciador de atividades"

#: powerdevilcore.cpp:437
#, kde-format
msgid "This activity's policies prevent the system from going to sleep"
msgstr ""
"As políticas desta atividade impedem que o sistema entre no modo de suspensão"

#: powerdevilcore.cpp:449
#, kde-format
msgid "This activity's policies prevent screen power management"
msgstr ""
"As políticas desta atividade impedem o gerenciamento de energia da tela"

#: powerdevilcore.cpp:512
#, kde-format
msgid "Extra Battery Added"
msgstr "Bateria extra adicionada"

#: powerdevilcore.cpp:513 powerdevilcore.cpp:729
#, kde-format
msgid "The computer will no longer go to sleep."
msgstr "O computador não entrará mais em suspensão."

#: powerdevilcore.cpp:573
#, kde-format
msgctxt "%1 is vendor name, %2 is product name"
msgid "%1 %2"
msgstr "%1 %2"

#: powerdevilcore.cpp:576
#, kde-format
msgctxt "The battery in an external device"
msgid "Device Battery Low (%1% Remaining)"
msgstr "Bateria do dispositivo fraca (%1% restantes)"

#: powerdevilcore.cpp:578
#, kde-format
msgctxt "Placeholder is device name"
msgid ""
"The battery in \"%1\" is running low, and the device may turn off at any "
"time. Please recharge or replace the battery."
msgstr ""
"A bateria no \"%1\" está fraca e o dispositivo pode se desligar a qualquer "
"momento. Recarregue ou substitua a bateria."

#: powerdevilcore.cpp:584
#, kde-format
msgid "Mouse Battery Low (%1% Remaining)"
msgstr "Bateria do mouse fraca (%1% restantes)"

#: powerdevilcore.cpp:588
#, kde-format
msgid "Keyboard Battery Low (%1% Remaining)"
msgstr "Bateria do teclado fraca (%1% restantes)"

#: powerdevilcore.cpp:592
#, kde-format
msgid "Bluetooth Device Battery Low (%1% Remaining)"
msgstr "Bateria do dispositivo Bluetooth fraca (%1% restantes)"

#: powerdevilcore.cpp:594
#, kde-format
msgctxt "Placeholder is device name"
msgid ""
"The battery in Bluetooth device \"%1\" is running low, and the device may "
"turn off at any time. Please recharge or replace the battery."
msgstr ""
"A bateria do dispositivo Bluetooth \"%1\" está fraca e o dispositivo pode se "
"desligar a qualquer momento. Recarregue ou substitua a bateria."

#: powerdevilcore.cpp:640
#, kde-format
msgid ""
"Battery running low - to continue using your computer, make sure that the "
"power adapter is plugged in and that it provides enough power."
msgstr ""
"A bateria está com pouca carga. Para continuar usando o computador, "
"certifique-se de que o adaptador de energia está conectado e que fornece "
"energia o suficiente."

#: powerdevilcore.cpp:642
#, kde-format
msgid ""
"Battery running low - to continue using your computer, plug it in or shut it "
"down and change the battery."
msgstr ""
"A bateria está com pouca carga. Para continuar usando o computador, conecte-"
"o a uma fonte de energia ou desligue-o e troque a bateria."

#: powerdevilcore.cpp:659
#, kde-format
msgctxt ""
"Cancel timeout that will automatically put system to sleep because of low "
"battery"
msgid "Cancel"
msgstr "Cancelar"

#: powerdevilcore.cpp:671
#, kde-format
msgid "Battery level critical. Your computer will shut down in 60 seconds."
msgstr ""
"O nível de carga da sua bateria está crítico. Seu computador será desligado "
"em 60 segundos."

#: powerdevilcore.cpp:672
#, kde-format
msgctxt ""
"@action:button Shut down without waiting for the battery critical timer"
msgid "Shut Down Now"
msgstr "Desligar agora"

#: powerdevilcore.cpp:677
#, kde-format
msgid ""
"Battery level critical. Your computer will enter hibernation mode in 60 "
"seconds."
msgstr ""
"O nível de carga da sua bateria está crítico. Seu computador entrará em modo "
"de hibernação em 60 segundos."

#: powerdevilcore.cpp:678
#, kde-format
msgctxt ""
"@action:button Enter hibernation mode without waiting for the battery "
"critical timer"
msgid "Hibernate Now"
msgstr "Hibernar agora"

#: powerdevilcore.cpp:683
#, kde-format
msgid "Battery level critical. Your computer will go to sleep in 60 seconds."
msgstr ""
"O nível de carga da sua bateria está crítico. Seu computador entrará em modo "
"de suspensão em 60 segundos."

#: powerdevilcore.cpp:684
#, kde-format
msgctxt ""
"@action:button Suspend to ram without waiting for the battery critical timer"
msgid "Sleep Now"
msgstr "Suspender agora"

#: powerdevilcore.cpp:689
#, kde-format
msgid "Battery level critical. Please save your work."
msgstr "O nível de carga da sua bateria está crítico. Salve seu trabalho."

#: powerdevilcore.cpp:700
#, kde-format
msgid "Battery Low (%1% Remaining)"
msgstr "Bateria fraca (%1% restantes)"

#: powerdevilcore.cpp:704
#, kde-format
msgid "Battery Critical (%1% Remaining)"
msgstr "Bateria em nível crítico (%1% restantes)"

#: powerdevilcore.cpp:728
#, kde-format
msgid "AC Adapter Plugged In"
msgstr "Adaptador de energia conectado"

#: powerdevilcore.cpp:731
#, kde-format
msgid "Running on AC power"
msgstr "Conectado ao adaptador de energia"

#: powerdevilcore.cpp:731
#, kde-format
msgid "The power adapter has been plugged in."
msgstr "O adaptador de energia foi conectado."

#: powerdevilcore.cpp:734
#, kde-format
msgid "Running on Battery Power"
msgstr "Usando a bateria"

#: powerdevilcore.cpp:734
#, kde-format
msgid "The power adapter has been unplugged."
msgstr "O adaptador de energia foi desconectado."

#: powerdevilcore.cpp:795
#, kde-format
msgid "Charging Complete"
msgstr "Carga completa"

#: powerdevilcore.cpp:795
#, kde-format
msgid "Battery now fully charged."
msgstr "A bateria está com carga completa."

#~ msgid "Can't open file"
#~ msgstr "Não foi possível abrir o arquivo"
