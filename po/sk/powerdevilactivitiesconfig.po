# translation of powerdevilactivitiesconfig.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2014, 2015, 2016.
# Mthw <jari_45@hotmail.com>, 2019.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: powerdevilactivitiesconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-20 00:47+0000\n"
"PO-Revision-Date: 2020-07-11 12:03+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com"

#: activitypage.cpp:72
#, kde-format
msgid ""
"The activity service is running with bare functionalities.\n"
"Names and icons of the activities might not be available."
msgstr ""
"Služba aktivít beží s obmedzenou funkcionalitou.\n"
"Názvy a ikony aktivít nemusia byť dostupné."

#: activitypage.cpp:146
#, kde-format
msgid ""
"The activity service is not running.\n"
"It is necessary to have the activity manager running to configure activity-"
"specific power management behavior."
msgstr ""
"Služba aktivít nebeží.\n"
"Je potrebné, aby správca aktivít bežal na nastavenie správania správy "
"napájania podľa aktivít."

#: activitypage.cpp:242
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Zdá sa, že služba správy napájania nie je spustená."

#: activitywidget.cpp:98
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Uspať"

#: activitywidget.cpp:102
#, kde-format
msgid "Hibernate"
msgstr "Hibernovať"

#: activitywidget.cpp:104
#, kde-format
msgid "Shut down"
msgstr "Vypnúť"

#: activitywidget.cpp:108
#, kde-format
msgid "PC running on AC power"
msgstr "PC beží napájané zo siete"

#: activitywidget.cpp:109
#, kde-format
msgid "PC running on battery power"
msgstr "PC beží na batériu"

#: activitywidget.cpp:110
#, kde-format
msgid "PC running on low battery"
msgstr "PC beží pri nízkej úrovni batérie"

#: activitywidget.cpp:139
#, kde-format
msgctxt "This is meant to be: Act like activity %1"
msgid "Activity \"%1\""
msgstr "Aktivita \"%1\""

#. i18n: ectx: property (text), widget (QRadioButton, noSettingsRadio)
#: activityWidget.ui:19
#, kde-format
msgid "Do not use special settings"
msgstr "Nepoužiť špeciálne nastavenia"

#. i18n: ectx: property (text), widget (QRadioButton, actLikeRadio)
#: activityWidget.ui:31
#, kde-format
msgid "Act like"
msgstr "Správať sa ako"

#. i18n: ectx: property (text), widget (QRadioButton, specialBehaviorRadio)
#: activityWidget.ui:60
#, kde-format
msgid "Define a special behavior"
msgstr "Definovať špeciálne správanie"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownScreenBox)
#: activityWidget.ui:72
#, kde-format
msgid "Never turn off the screen"
msgstr "Nikdy nevypnúť obrazovku"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownPCBox)
#: activityWidget.ui:79
#, kde-format
msgid "Never shut down the computer or let it go to sleep"
msgstr "Nikdy nevypnúť počítač alebo nechať ho prejsť do spánku"

#. i18n: ectx: property (text), widget (QCheckBox, alwaysBox)
#: activityWidget.ui:91
#, kde-format
msgid "Always"
msgstr "Vždy"

#. i18n: ectx: property (text), widget (QLabel, alwaysAfterLabel)
#: activityWidget.ui:101
#, kde-format
msgid "after"
msgstr "po"

#. i18n: ectx: property (suffix), widget (QSpinBox, alwaysAfterSpin)
#: activityWidget.ui:108
#, kde-format
msgid " min"
msgstr " min"

#. i18n: ectx: property (text), widget (QRadioButton, separateSettingsRadio)
#: activityWidget.ui:138
#, fuzzy, kde-format
#| msgid "Use separate settings (advanced users only)"
msgid "Use separate settings"
msgstr "Použiť oddelené nastavenia (pokročilí používatelia)"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Zdá sa, že služba správy napájania nebeží.\n"
#~ "Toto môžte vyriešiť jej spustením alebo naplánovnaím v \"Spustenie a "
#~ "vypnutie\""

#~ msgid "Suspend"
#~ msgstr "Uspať"

#~ msgid "Never suspend or shutdown the computer"
#~ msgstr "Nikdy nezastaviť alebo nevypnúť počítač"

#~ msgid "Activities Power Management Configuration"
#~ msgstr "Nastavenie správy aktivít napájania"

#~ msgid "A per-activity configurator of KDE Power Management System"
#~ msgstr "Nastavenie KDE systému správy napájania založené na aktivite"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can fine tune power management settings for each of "
#~ "your activities."
#~ msgstr ""
#~ "Z tohto modulu môžete jemne nastaviť nastavenia správy napájania pre "
#~ "každú vašu aktivitu."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Správca"
